using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBase : MonoBehaviour
{
    public eCHARACTERTYPE m_CharacterType;
    public void AutoMove(Vector3 _vPos)//이동할 위치
    {
        Vector3 vec = Vector3.left * Time.fixedDeltaTime * 1.0f;

        transform.position += vec;
    }
    public AI_base m_AI = new AI_base();
    // Start is called before the first frame update
    private void Start()
    {
        m_AI.Init(this, 2f); //2초후에 생성
    }
    private void FixedUpdate()
    {
        if (null !=  m_AI)
            m_AI.UpdateState();
    }
    // Update is called once per frame
    private void Update()
    {
        
    }
}
