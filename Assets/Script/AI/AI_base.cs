using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Assets.HeroEditor.Common.CharacterScripts;

public class AI_base
{
   
    public Transform TARGETPOS;
    eAI m_eAI = eAI.eAI_END;

    CharacterBase m_chracaterBase = null;

    float m_fCreateTime;
    float m_fCurTiem;

   
    void SetCreate() //생성 or 작동
    {
        m_eAI = eAI.eAI_MOVE;
      
    }
    public void Init(CharacterBase _characterBase,float _fCreateTime) //초기화
    {
        m_chracaterBase = _characterBase;

    
        m_fCreateTime = _fCreateTime;
        m_fCurTiem = 0f;
    }
    public void UpdateState() //상태
    {
        if(m_fCurTiem < m_fCreateTime)
        {
         m_fCurTiem += Time.deltaTime;
            return;
        }     
        switch (m_eAI)
        {
            case eAI.eAI_CREATE:
                SetCreate();
                break;
            case eAI.eAI_MOVE:
                Setmove();
                break;
            case eAI.eAI_END:
                m_eAI = eAI.eAI_CREATE;
                   break;
        }
    
    }
   
    void Setmove()
    {
        m_chracaterBase.AutoMove(TARGETPOS.position);
    }
   
}