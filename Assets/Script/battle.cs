using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class battle : MonoBehaviour
{
    public CharacterBase PLAYER;
    public CharacterBase MONSTER;

    public Transform PLAYERPS;
    public Transform MONSTERPOS;
    private void Start()
    {
        PLAYER.m_AI.TARGETPOS = MONSTERPOS;
        MONSTER.m_AI.TARGETPOS = PLAYERPS;
    }
}
